// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

    // Constants
    @8192    // VRAM=8K
    D=A
    @SCREEN
    D=D+A
    @endscreen
    M=D      // endscreen=SCREEN+8K

(MAIN)
    @count
    M=0      // set count to 0
    @KBD
    D=M
    @WHITESCREEN
    D;JEQ
    @BLACKSCREEN
    0;JMP

(WHITESCREEN)
    @SCREEN
    D=A      // D=@SCREEN
    @count
    D=D+M    // D=@SCREEN+count
    @endscreen
    D=D-M    // D=@SCREEN+count-endscreen
    @MAIN
    D;JEQ    // if D==0 GOTO MAIN
    @endscreen
    D=D+M    // D=@SCREEN+count
    A=D
    M=0
    @count
    M=M+1
    @WHITESCREEN
    0;JMP

(BLACKSCREEN)
    @SCREEN
    D=A      // D=@SCREEN
    @count
    D=D+M    // D=@SCREEN+count
    @endscreen
    D=D-M    // D=@SCREEN+count-endscreen
    @MAIN
    D;JEQ    // if D==0 GOTO MAIN
    @endscreen
    D=D+M    // D=@SCREEN+count
    A=D
    M=-1
    @count
    M=M+1
    @BLACKSCREEN
    0;JMP
