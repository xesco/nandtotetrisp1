// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

    @R0
    D=M
    @i
    M=D    // i=R0
    @R1
    D=M
    @b
    M=D    // b=R1
    @prod
    M=0    // prod=0

(LOOP)
    @i
    D=M
    @FIN
    D;JLE  // Jump if i<=0
    
    D=D-1
    @i
    M=D    // i=i-1
    
    @b
    D=M
    @prod
    M=M+D  // prod=prod+R1
    
    @LOOP
    0;JMP

(FIN)
    @prod
    D=M
    @R2
    M=D    // R2=prod

(END)
    @END
    0;JMP
