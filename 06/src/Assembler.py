#!/usr/bin/env python

import sys
import argparse

from lib.assembler import Assembler as HackAss


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="Hack assembly file")
    args = parser.parse_args()

    name = args.filename.rsplit('.', 1)[0]
    ext = args.filename.rsplit('.', 1)[-1]

    if ext != "asm":
        sys.exit("Error: extension must be .asm")
    try:
        fd_input = open(args.filename, 'rt')
    except IOError:
        sys.exit("Can't open input file: {}".format(args.filename))
    try:
        fd_output = open(".".join([name, 'hack']), 'wt')
    except IOError:
        sys.exit("Can't open output file file: {}.{}".format(name, 'haxk'))

    ass = HackAss(fd_input)
    ass.first_pass()

    for inst in ass.second_pass():
        fd_output.write(inst)
        fd_output.write('\n')

    fd_input.close()
    fd_output.close()
