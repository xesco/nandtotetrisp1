from lib.code import Code
from lib.parser import Parser
from lib.static import VAR_ALLOC_START
from lib.symtable import SymbolTable


class Assembler:
    def __init__(self, fp):
        self.parser = Parser(fp)  # parser
        self.code = Code()        # code generator
        self.sym = SymbolTable()  # symbol table handler
        self.free = VAR_ALLOC_START  # var allocator init address

    def first_pass(self):
        p = self.parser
        s = self.sym
        while p.hasMoreCommands():
            p.advance()
            if p.is_L_type:
                sym = p.symbol
                if not s.contains(sym):
                    s.addEntry(sym, p.n+1)
        # reset file pointer position
        p.reset()

    def second_pass(self):
        p = self.parser
        while p.hasMoreCommands():
            p.advance()
            if p.is_A_type:
                addr = self.process_A_instr()
                yield bitsAtype(addr)
            elif p.is_C_type:
                dest, comp, jump = self.process_C_instr()
                yield bitsCtype(dest, comp, jump)

    def process_A_instr(self):
        p = self.parser
        s = self.sym

        sym = p.symbol
        if sym.isdigit():
            return int(sym)
        if s.contains(sym):
            return s.getAddress(sym)

        addr = self.free
        s.addEntry(sym, addr)
        self.free += 1
        return addr

    def process_C_instr(self):
        p = self.parser
        c = self.code
        return c.dest(p.dest), c.comp(p.comp), c.jump(p.jump)


def bitsAtype(addr):
    return "0{:015b}".format(addr)


def bitsCtype(dest, comp, jump):
    return "111{}{}{}".format(comp, dest, jump)
