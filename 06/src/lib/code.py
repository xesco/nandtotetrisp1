from lib.static import DEST, COMP, JUMP, NULL


class Code:
    def dest(self, dest_):
        if not dest_:
            return NULL
        return DEST[dest_]

    def comp(self, comp_):
        return COMP[comp_]

    def jump(self, jump_):
        if not jump_:
            return NULL
        return JUMP[jump_]
