from lib.static import TYPE


class Parser:
    def __init__(self, fp=None):
        self.fp = fp              # file pointer
        self.current_line = None  # string holding a Hack command
        self.current_cmd = None   # clean command from input
        self.n = -1               # current line

    def hasMoreCommands(self):
        for line in self.fp:
            line = line.strip()
            if line and not line.startswith("//"):
                self.current_line = line
                return True
        return False

    def advance(self):
        cmd = self.current_line.split("//").pop(0)  # strip comment
        self.current_cmd = "".join(cmd.split())     # delete blanks
        if not self.is_L_type:  # increment line counter
            self.n += 1

    def reset(self):
        self.fp.seek(0)

    @property
    def commandType(self):
        if self.current_cmd.startswith('@'):
            return TYPE['A']
        elif self.current_cmd.startswith('('):
            return TYPE['L']
        else:
            return TYPE['C']

    @property
    def symbol(self):
        if self.is_A_type:
            return self.current_cmd[1:]    # strip @ sign
        if self.is_L_type:
            return self.current_cmd[1:-1]  # strip () signs

    # the following dest, comp and jump return None if not applicable
    # the code later checks for None and translate it to NULL i.e. "000"

    @property
    def dest(self):
        if '=' in self.current_cmd:
            return self.current_cmd.split('=').pop(0)

    @property
    def comp(self):
        cmd = self.current_cmd.split(';').pop(0)  # check if there's a jump
        if '=' in cmd:                            # check if there's a dest
            return cmd.split('=').pop(1)
        else:
            return cmd

    @property
    def jump(self):
        if ';' in self.current_cmd:
            return self.current_cmd.split(';').pop(1)

    # helpers to get instruction type

    @property
    def is_A_type(self):
        return self.commandType == TYPE['A']

    @property
    def is_C_type(self):
        return self.commandType == TYPE['C']

    @property
    def is_L_type(self):
        return self.commandType == TYPE['L']
