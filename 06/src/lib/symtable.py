from lib.static import SYMT


class SymbolTable:
    def __init__(self):
        self.symtable = SYMT

    def addEntry(self, sym, addr):
        self.symtable[sym] = addr

    def contains(self, sym):
        return sym in self.symtable

    def getAddress(self, sym):
        return self.symtable.get(sym)
